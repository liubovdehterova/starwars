const main = document.querySelector('.main');
const preloader = document.querySelector('.lds-ripple');
preloader.style.display = 'block';

fetch('https://ajax.test-danit.com/api/swapi/films')
    .then(res => res.json())
    .then(data => {
        data.forEach(({characters, name, episodeId, openingCrawl}) => {
            const div = document.createElement('div');
            const p = document.createElement('p');
            const pDes = document.createElement('p');
            div.append(p);
            p.classList.add('title');
            pDes.classList.add('main__info');
            div.append(pDes);
            p.innerHTML = `
                            Episode Number:
                            <strong>${episodeId}</strong>, 
                            <br>
                            Movie Name:
                            <strong>${name}</strong>
                           `;
            pDes.innerHTML = openingCrawl;
            const list = document.createElement('ul');
            div.append(list);
            main.append(div);
            preloader.style.display = 'none';
            characters.forEach(elem => {
                fetch(`${elem}`)
                    .then(res => res.json())
                    .then(data => {
                        const listLi = document.createElement('li');
                        listLi.classList.add('people__info');
                        listLi.innerHTML = data.name;
                        list.append(listLi);
                    });
            });
        });
    })
    .catch(error => {
        console.error('Error fetching film data:', error);
        preloader.style.display = 'none';
    });